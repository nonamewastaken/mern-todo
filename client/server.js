//THIS SCRIPT IS ONLY SUPPOSED TO SERVE THE BUILT PRODUCT; IT WILL NOT WORK; UNLESS YOU BUILD THE PRJECT FIRST
const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(3000);