import React, { useState } from 'react';
import LoginForm from './components/LoginForm';
import SignupForm from './components/SignupForm';
import TodoCreator from './components/TodoCreator';
import TodoList from './components/TodoList';
const cfg = require('./config.json');
const crypto = require('crypto');
var initLoad = false;


function App() {

  const [GlobalInfo, setGlobalInfo] = useState("");

  //USER/ERROR objects
  const [LoginStatus, setLoginStatus] = useState("");
  const [SignupStatus, setSignupStatus] = useState("");
  const [user, setUser] = useState({ username: "", email: "", token: "", user_id: "", max_todos: 0 })

  //SIGNUP/LOGIN SECTION
  const Login = async (details) => {

    //hash password
    var shasum = crypto.createHash('sha512');
    shasum.update(details.password);
    const passwordHash = shasum.digest('hex');

    //make login request to backend
    var response;
    await fetch(`${cfg.backend_url}login/?email=${encodeURIComponent(details.email)}&password_hash=${encodeURIComponent(passwordHash)}`)
      .then(response => response.json())
      .then(data => response = data);

    //TEMPORARY; FOR TESTING ONLY
    if (response.status === "success") {
      setUser({
        username: response.username,
        email: details.email,
        token: response.token,
        user_id: response.user_id,
        max_todos: response.max_todos
      })

      console.log(response)

    } else {
      setLoginStatus(response.status);

      //clear error after 3 sec
      setTimeout(function () {
        setLoginStatus("");
      }, 3000);
    }

  }

  const Signup = async (details) => {
    if (details.password !== details.password_repeat) {
      setSignupStatus("Password does not match");

      //clear error after 3 sec
      setTimeout(function () {
        setSignupStatus("");
      }, 3000);
    } else {
      //do the signup part

      //hash password as sha512
      var shasum = crypto.createHash('sha512');
      shasum.update(details.password);
      const passwordHash = shasum.digest('hex');

      //make request to backend-api
      var response;
      await fetch(`${cfg.backend_url}signup`, {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: details.email, username: details.username, password_hash: passwordHash })
      })
        .then(res => res.json())
        .then(res => { response = res.status; console.log(res) })

      setSignupStatus(response);

      //clear error after 3 sec
      setTimeout(function () {
        setSignupStatus("");
      }, 3000);
    }
  }

  const Logout = async () => {
    //resets current user variable to empty
    setUser({ username: "", email: "", token: "", user_id: "", max_todos: 0 });
    setTodos([]);
  }
  //SIGNUP/LOGIN SECTION END
  const [todos, setTodos] = useState([]);

  const GetTodos = async () => {
    var response;
    await fetch(`${cfg.backend_url}gettodos/?token=${encodeURIComponent(user.token)}&user_id=${encodeURIComponent(user.user_id)}`)
      .then(response => response.json())
      .then(data => response = data);
    setTodos(response.todos);
    console.log(response)
  }

  const CreateTodo = async (details) => {
    var response;
    await fetch(`${cfg.backend_url}createtodo`, {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user_id: user.user_id, token: user.token, title: details.title, content: details.content })
    })
      .then(res => res.json())
      .then(res => { response = res.status; console.log(res) })

    setGlobalInfo(response);

    //TODO: Make this more efficient (delete directly from todo object, do not make extra query):
    GetTodos();

    //clear info after 3 sec
    setTimeout(function () {
      setGlobalInfo("");
    }, 3000);
  }

  const UpdateTodo = async (ev) => {
    const tid = ev.target.getAttribute("todo_id")
    console.log("Updating", tid)

    var response;
    await fetch(`${cfg.backend_url}updatetodo`, {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user_id: user.user_id, token: user.token, todo_id: tid, new_state:ev.target.checked})
    })
      .then(res => res.json())
      .then(res => { response = res.status; console.log(res) })

    setGlobalInfo(response);

    //TODO: Make this more efficient (delete directly from todo object, do not make extra query):
    GetTodos();

    //clear info after 3 sec
    setTimeout(function () {
      setGlobalInfo("");
    }, 3000);
  }

  const DeleteTodo = async (ev) => {
    const tid = ev.target.getAttribute("todo_id")
    console.log("Deleting", tid)

    var response;
    await fetch(`${cfg.backend_url}deletetodo`, {
      method: 'delete',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user_id: user.user_id, token: user.token, todo_id: tid })
    })
      .then(res => res.json())
      .then(res => { response = res.status; console.log(res) })

    setGlobalInfo(response);

    //TODO: Make this more efficient (delete directly from todo object, do not make extra query):
    GetTodos();

    //clear info after 3 sec
    setTimeout(function () {
      setGlobalInfo("");
    }, 3000);

  }


  //loads todos once, after successful login
  if (user.email !== "" && !initLoad) {
    GetTodos();
    initLoad = true;
  }

  //stores if creator is shown
  const [creatorShown, setCreatorShown] = useState(false);
  const toggleCreator = () => {
    setCreatorShown(!creatorShown);
  }

  return (
    <div className="App">

      {/*TODO: ping backend, error if backend is down*/}

      {(user.email !== "") ? ( //check if user is already logged in

        //SUCCESSFUL LOGIN SCREEN

        <div>

          <div className="welcomeText">

            <div>
              <h2>Welcome, <span>{user.username}</span></h2> {/*display welcome screen*/}
            </div>

            {/*DISPLAY VALUE OF GLOBAL INFO IF NOT EMPTY*/}
            <h2>
              {(GlobalInfo !== "") ? (<div className="status">{GlobalInfo}</div>) : (<div className="status"></div>)}
            </h2>

          </div>


          {/*Load and render todos*/}
          <div className="todo-container">
            <TodoList todos={todos} UpdateTodo={UpdateTodo} DeleteTodo={DeleteTodo} />
          </div>

          {(creatorShown) ? (
            <TodoCreator CreateTodo={CreateTodo} />
          ) : (
            ""
          )
          }

          <footer>
            {/*TODO: HAS TO BE PUT INTO A FANCY MENU*/}
            <button className="menuButton" onClick={Logout}><img src={require("./static/icons/logout_white_24dp.svg").default} alt={"LOGOUT"} /></button>
            <button className="menuButton" onClick={GetTodos}><img src={require("./static/icons/refresh_white_24dp.svg").default} alt={"REFRESH"} /></button>
            <button className="menuButton" onClick={toggleCreator}><img src={require("./static/icons/note_add_white_24dp.svg").default} alt={"CREATE NEW"} /></button>
          </footer>
        </div>

      ) : (
        //NOT LOGGED IN SCREEN (Login and signup option)
        <div>
          <LoginForm Login={Login} status={LoginStatus} GetTodos={GetTodos} id="loginForm" /> {/*pass login and error function, render login form*/}
          <div id="loginSpacer" />
          <SignupForm Signup={Signup} status={SignupStatus} id="signupForm" />
        </div>
      )}

    </div>
  );
}

export default App;
