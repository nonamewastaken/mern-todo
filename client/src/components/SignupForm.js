import React, { useState } from 'react'

function SignupForm({ Signup, status }) {
    const [details, setDetails] = useState({ email: "", password: "", username:"", password_repeat:"" });

    const submitHandler = e => {
        e.preventDefault();

        Signup(details) //passes details to signup function in app.js
    }

    return (
        <form className="logupForm" onSubmit={submitHandler /*run submitHandler() if submit is pressed*/}>
            <div className="form-inner">
                <h2>SIGNUP</h2>
                {status !== "" ? (<div className="status">{status}</div>) : ""}
                <div className="formGroup">
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" onChange={e => setDetails({ ...details, email: e.target.value })/*Set values in details var if content changes*/} value={details.email} />
                </div>
                <div className="formGroup">
                    <label htmlFor="username">Username:</label>
                    <input type="text" name="username" id="username" onChange={e => setDetails({ ...details, username: e.target.value })} value={details.username} />
                </div>
                <div className="formGroup">
                    <label htmlFor="password">Password:</label>
                    <input type="password" name="password" id="password" onChange={e => setDetails({ ...details, password: e.target.value })} value={details.password} />
                </div>
                <div className="formGroup">
                    <label htmlFor="password_repeat">Repeat password:</label>
                    <input type="password" name="password_repeat" id="password_repeat" onChange={e => setDetails({ ...details, password_repeat: e.target.value })} value={details.password_repeat} />
                </div>
                <input type="submit" value="SIGNUP" id="submit" />
            </div>
        </form>
    )
}

export default SignupForm
