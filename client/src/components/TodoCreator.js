import React,{ useState } from 'react'

function TodoCreator({CreateTodo , status}) {
    const [details, setDetails] = useState({ title: "", content: ""});

    const submitHandler = e => {
        e.preventDefault();

        CreateTodo(details);
    }

    return (
        <form onSubmit={submitHandler} className="todoCreator">
        <div className="form-inner">
            <h2>Create todo</h2>
            {status !== "" ? (<div className="status">{status}</div>) : ""}
            <div className="formGroup">
                <label htmlFor="title">Title:</label>
                <input type="text" name="title" id="title" onChange={e => setDetails({ ...details, title: e.target.value })/*Set values in details var if content changes*/} value={details.title} />
            </div>
            <div className="formGroup">
                <label htmlFor="content">Content:</label>
                <input type="text" name="content" id="content" onChange={e => setDetails({ ...details, content: e.target.value })} value={details.content} />
            </div>
            <input type="submit" value="CREATE" id="submit" />
        </div>
    </form>
    )
}

export default TodoCreator
