import React, { useState } from 'react'

function LoginForm({ Login, status }) {
    const [details, setDetails] = useState({ email: "", password: "" });

    const submitHandler = e => {
        e.preventDefault();

        Login(details) //passes details to login function in app.js
    }

    return (
        <form onSubmit={submitHandler /*run submitHandler() if submit is pressed*/} className="logupForm">
            <div className="form-inner">
                <h2>LOGIN</h2>
                {status !== "" ? (<div className="status">{status}</div>) : ""}
                <div className="formGroup">
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" onChange={e => setDetails({ ...details, email: e.target.value })/*Set values in details var if content changes*/} value={details.email} />
                </div>
                <div className="formGroup">
                    <label htmlFor="password">Password:</label>
                    <input type="password" name="password" id="password" onChange={e => setDetails({ ...details, password: e.target.value })/*Set values in details var if content changes*/} value={details.password} />
                </div>
                <input type="submit" value="LOGIN" id="submit"/>
            </div>
        </form>
    )
}

export default LoginForm
