import React from 'react'


function TodoList({ todos, UpdateTodo, DeleteTodo }) {

    return todos.map((todo, index) => (

        <div className="todoItem" key={todo.todo_id}>

            <h3>{todo.title}</h3>
            <p>{todo.content}</p>

            <div className="buttonComtainer">
                <button onClick={DeleteTodo} todo_id={todo.todo_id}></button>

                <input
                    onClick={UpdateTodo}
                    className="todoCheckBox"
                    type="checkbox"
                    todo_id={todo.todo_id}
                    defaultChecked={todo.done}
                />
            </div>

        </div>
    ))

}

export default TodoList