var express = require('express')
const app = express();
const mongo = require('./database/db.js');
const port = 4000;

//SET UP DB
async function setup() {
    //Connect to db (for checking if errors occur)
    await mongo.init();

    app.use(express.urlencoded({ extended: true }));
    app.use(express.json())

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods','DELETE, GET, POST, OPTIONS, HEAD');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        next();
    })

    app.use('/', require('./routes/root'));
    app.use('/', require('./routes/login'));
    app.use('/', require('./routes/signup'));
    app.use('/', require('./routes/gettodos'));
    app.use('/', require('./routes/updatetodo'));
    app.use('/', require('./routes/deleteuser'));
    app.use('/', require('./routes/deletetodo'));
    app.use('/', require('./routes/createtodo'));

    // Error handlers
    app.use(function fourOhFourHandler(req, res) {
        res.status(404).send()
    })
    app.use(function fiveHundredHandler(err, req, res, next) {
        console.error(err)
        res.status(500).send()
    })

    //start server
    app.listen(port, () => {
        console.log(`Started server on port ${port}`)
    });
}
setup();