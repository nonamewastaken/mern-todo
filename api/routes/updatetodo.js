var express = require('express');
var router = express.Router();
const cfg = require('../config/config.json');
var mongo = require('../database/db');

router.post('/updatetodo', async function(req, res, next) {

    const body = req.body;

    const necessaryDataMissing = Boolean( //allows empty title or body
        body.token == undefined
        || body.user_id == undefined
        || body.todo_id == undefined
        || body.new_state == undefined
    );

    if (necessaryDataMissing) {
        res.status(422).json({ status: "data_missing" });
        return;
    }

    const lengthInvalid = Boolean(
        body.user_id.length != 40
        || body.token.length != 172
        || body.todo_id.length != cfg.todo_id_length
    );

    if (lengthInvalid) {
        res.status(422).json({ status: "data_does_not_comply_with_size_restrictions" });
        return;
    }

    //check if user exists
    if (await mongo.users.checkForUserId(body.user_id) === 0) {
        res.status(404).json({ status: "user_does_not_exist" });
        return;
    }

    //check user creds, check if todo exists, delete todo, decrement current_todos, success

    //get user object from db
    var user = await mongo.users.getUser(body.user_id)

    //check token
    if (body.token !== user.token) {
        res.status(403).json({ status: "invalid_token" });
        return;
    }

    if (await mongo.todos.checkForTodoId(body.todo_id) === 0) {
        res.status(404).json({ status: "todo_does_not_exist" });
        return;
    }

    //Update the todo's state (body.new_state)
    mongo.todos.collection.updateOne({ todo_id: body.todo_id }, { $set: { done: body.new_state } })

    res.status(200).json({ status: "success"});

});

module.exports = router;