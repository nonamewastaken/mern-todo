var express = require('express');
var mongo = require('../database/db');
var router = express.Router();
var crypto = require('crypto')

router.get('/login', async function (req, res, next) {

    //check that all necessary data is provided by endpoint
    const necessaryDataMissing = Boolean(
        req.query.email == ""
        || req.query.email == undefined
        || req.query.password_hash == undefined
    );

    if (necessaryDataMissing) {
        res.status(422).json({ status: "data_missing" });
        return;
    }

    const lengthInvalid = Boolean(
        req.query.email.length > 100
        || req.query.password_hash.length != 128 //IMPORTANT: Assumes sha512 hash is in hexadecimal form
    );

    if (lengthInvalid) {
        res.status(422).json({ status: "data_does_not_comply_with_size_restrictions" });
        return;
    }

    //generate user id from base64 encoded email
    var shasum = crypto.createHash('sha1');
    shasum.update(tobase64(req.query.email));
    const uid = shasum.digest('hex');

    //Check if user exists
    if (await mongo.users.checkForUserId(uid) === 0) {
        res.status(404).json({ status: "user_does_not_exist" });
        return;
    }

    //get user object from db
    var user = await mongo.users.getUser(uid)

    //read salt from user object, generate hash from query hash (directly, no base64) using salt, compare to hash from db
    var hash = crypto.createHmac('sha512', user.salt)
    hash.update(req.query.password_hash)
    var value = hash.digest('hex')

    //send error if password is hash does not match, return
    if (value !== user.password_hash) {
        res.status(403).json({ status: "invalid_password" });
        return;
    }

    //generate new access token (automatically loggs all other sessions out) //TODO: make it not logg out other sessions
    var token = crypto.randomBytes(128).toString('base64'); //172 characters long random string

    //set token in db, respond 200 with token
    mongo.users.collection.updateOne({ user_id: uid }, { $set: { token: token } })

    res.status(200).json({ status: "success", token: token, user_id: uid, max_todos: user.max_todos, username: fromBase64(user.username) });

});

function tobase64(string) {
    let bufferObj = Buffer.from(string, "utf8");
    return bufferObj.toString("base64");
}

function fromBase64(base64) {
    const buff = Buffer.from(base64, 'base64');
    return buff.toString('utf-8');
}

module.exports = router;