var express = require('express');
const mongo = require('../database/db');
var router = express.Router();

//Present error to user if root is called, as it has no use
router.get('/', function(req, res, next) {
    //TODO: add option to make this url redirect to the web-app
    res.send("<h1>This is the backend url of the service you are trying to reach, contact whoever gave you this link and ask them for a correct one!<h1>");
});

module.exports = router;