var express = require('express');
var router = express.Router();
var mongo = require('../database/db');

router.get('/gettodos', async function (req, res, next) {


    //check that all necessary data is provided by endpoint
    const necessaryDataMissing = Boolean(
        req.query.token == undefined
        || req.query.user_id == undefined
    );

    if (necessaryDataMissing) {
        res.status(422).json({ status: "data_missing" });
        return;
    }

    const lengthInvalid = Boolean(
        req.query.token.length != 172
        || req.query.user_id.length != 40
    );

    if (lengthInvalid) {
        res.status(422).json({ status: "data_does_not_comply_with_size_restrictions" });
        return;
    }


    //Check if user exists
    if (await mongo.users.checkForUserId(req.query.user_id) === 0) {
        res.status(404).json({ status: "user_does_not_exist" });
        return;
    }

    //get user object from db
    var user = await mongo.users.getUser(req.query.user_id)

    //check token
    if (req.query.token !== user.token) {
        res.status(403).json({ status: "invalid_token" });
        return;
    }

    //query todos
    var todos;
    await mongo.todos.collection.find({ owner: req.query.user_id }).project({ _id: 0, owner: 0 }).toArray()
        .then(function (result) {
            todos = result;
        })


    for (var i = 0; i < todos.length; i++) {
        todos[i].title = fromBase64(todos[i].title);
        todos[i].content = fromBase64(todos[i].content);
    }

    res.status(200).json({ status: "success", todos: todos });

});

function fromBase64(base64) {
    const buff = Buffer.from(base64, 'base64');
    return buff.toString('utf-8');
}

module.exports = router;