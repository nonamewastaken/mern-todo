var express = require('express');
var crypto = require('crypto')
var mongo = require('../database/db');
var router = express.Router();
const cfg = require('../config/config.json');

router.post('/signup', async function (req, res, next) {
    //mongo.users.addUser({email:"test", username:"test"}); //example use for the adduser function
    const body = req.body;

    //Validate email and check if all necessary data is present
    //If any issue is found, return 422
    const necessaryDataMissing = Boolean(
        body.email == ""
        || body.username == ""
        || body.password_hash == ""
        || body.email == undefined
        || body.username == undefined
        || body.password_hash == undefined
    );

    if (necessaryDataMissing) {
        res.status(422).json({ status: "data_missing" });
        return;
    }

    const lengthInvalid = Boolean(
        body.email.length > 100
        || body.username.length > 30
        || body.password_hash.length != 128 //IMPORTANT: Assumes sha512 hash is in hexadecimal form
    );

    if (lengthInvalid) {
        res.status(422).json({ status: "data_does_not_comply_with_size_restrictions" });
        return;
    }

    if (!validateEmail(body.email)) {
        res.status(422).json({ status: "invalid_email" });
        return;
    }

    //encode everything to base64, store in an object for later use. Token field added as placeholder
    var user = { email: tobase64(body.email), username: tobase64(body.username), token: "" };

    //check for duplicates in db ==> if username/mail is duplicate return error
    //does not search by uid, as username is also of importance
    if (await mongo.users.checkForUser(user.username, user.email) === 1) {
        res.status(409).json({ status: "user_already_exists" });
        return;
    }

    //Hash user-provided password-hash using salted sha512, apped eresult to user object
    var salt = crypto.randomBytes(128).toString('base64');

    var hash = crypto.createHmac('sha512', salt)
    hash.update(body.password_hash)
    var value = hash.digest('hex')

    user = Object.assign({ password_hash: value, salt: salt }, user);

    //sha1 hash base64 encoded email to generate user id [not using a random id, as it would require additional db calls to check for duplicates].
    //Aditionally adds timestamp (unix time), max todo ammount and current todo ammount(0)
    var shasum = crypto.createHash('sha1');
    shasum.update(user.email);
    const uid = shasum.digest('hex');

    //Append previously generated data to the user object
    user = Object.assign({ creation_timestamp: Date.now(), user_id: uid, max_todos: cfg.max_todos, current_todos: 0 }, user);

    //write to db
    mongo.users.addUser(user);

    res.status(200).json({ status: "success" });

});

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function tobase64(string) {
    let bufferObj = Buffer.from(string, "utf8");
    return bufferObj.toString("base64");
}

module.exports = router;