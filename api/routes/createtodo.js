var express = require('express');
var mongo = require('../database/db');
var crypto = require('crypto')
var router = express.Router();
const cfg = require('../config/config.json');

router.post('/createtodo', async function (req, res, next) {

    const body = req.body;

    const necessaryDataMissing = Boolean( //allows empty title or body
        body.token == undefined
        || body.user_id == undefined
        || body.content == undefined
        || body.title == undefined
    );

    if (necessaryDataMissing) {
        res.status(422).json({ status: "data_missing" });
        return;
    }

    const lengthInvalid = Boolean(
        body.user_id.length != 40
        || body.token.length != 172
        || body.content.length > cfg.max_content_length
        || body.title.length > cfg.max_title_length
    );

    if (lengthInvalid) {
        res.status(422).json({ status: "data_does_not_comply_with_size_restrictions" });
        return;
    }

    //check if user exists
    if (await mongo.users.checkForUserId(body.user_id) === 0) {
        res.status(404).json({ status: "user_does_not_exist" });
        return;
    }

    //get user object ==> check if token is correct ==> check if max todo ammount is already satisfied

    //get user object from db
    var user = await mongo.users.getUser(body.user_id)

    //check token
    if (body.token !== user.token) {
        res.status(403).json({ status: "invalid_token" });
        return;
    }

    if ((user.current_todos + 1) > user.max_todos) {
        res.status(403).json({ status: "maximum_ammount_of_todos_exceeded" });
        return;
    }

    //generate random id, until one not present in db is found
    var chosen = false;
    var tid;
    while (!chosen) {
        tid = crypto.randomBytes(cfg.todo_id_length / 2).toString('hex');
        if (await mongo.todos.checkForTodoId(tid) === 0) {
            chosen = true;
        }
    }

    //create new todo obj., write to db
    var todo = { owner: user.user_id, title: tobase64(body.title), content: tobase64(body.content), done: false, creation_timestamp: Date.now(), todo_id: tid }
    await mongo.todos.addTodo(todo)

    //update ammount of todos user owns in db
    await mongo.users.collection.updateOne({ user_id: user.user_id }, { $set: { current_todos: user.current_todos + 1 } })

    res.status(200).json({ status: "success", todo_id: todo.todo_id, current_todos: user.current_todos + 1 })

});

function tobase64(string) {
    let bufferObj = Buffer.from(string, "utf8");
    return bufferObj.toString("base64");
}

function frombase64(base64) {
    const buff = Buffer.from(base64, 'base64');
    return buff.toString('utf-8');
}

module.exports = router;