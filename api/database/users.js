const config = require('../config/db.json');

class user {
    constructor(db) {
        this.collection = db.collection(config.user_collection);
    }
    //checks if a username or email already exists in the db
    async checkForUser(b64username, b64email) {
        //check if either email or username is already in db, returns promise (has to be dissassembled by using .then())
        var present = 1;
        await this.collection.count({ "$or": [{ username: b64username }, { email: b64email }] }, { limit: 1 })
            .then(function (result) {
                present = result;
            })
        return present;
    }
    //checks if a userid is already present in the db
    async checkForUserId(uid) {
        //check if uid is already present in db
        var present = 1;
        await this.collection.count({ user_id: uid }, { limit: 1 })
            .then(function (result) {
                present = result;
            })
        return present;
    }
    //Function to get all user data by uid
    async getUser(uid) {
        var user;
        await this.collection.findOne({ user_id: uid })
            .then(function (result) {
                user = result;
            })
        return user;
    }
    //function to add user to db (requires correctly formatted object as input)
    async addUser(user) {
        const newUser = await this.collection.insertOne(user);
        return newUser;
    }
}
module.exports = user;