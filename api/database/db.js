const { MongoClient } = require('mongodb');
const users = require('./users');
const todos = require('./todos');
const config = require('../config/db.json');

class mongo {
    constructor() {
        this.client = new MongoClient(config.db_uri);
    }
    async init() {
        await this.client.connect();
        console.log('connected');

        this.db = this.client.db("TodoApp");
        this.users = new users(this.db);
        this.todos = new todos(this.db);
    }
}

module.exports = new mongo();