const config = require('../config/db.json');

class todo {
    constructor(db) {
        this.collection = db.collection(config.todo_collection);
    }
    async addTodo(todo) {
        const newTodo = await this.collection.insertOne(todo);
        return newTodo;
    }
    async checkForTodoId(tid) {
        //check if tid is already present in db
        var present = 1;
        await this.collection.count({ todo_id: tid }, { limit: 1 })
            .then(function (result) {
                present = result;
            })
        return present;
    }
}
module.exports = todo;